﻿#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    socket = nullptr;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_connectPushButton_clicked()
{
    if (!socket) {
        socket = new QTcpSocket();
        socket->connectToHost(ui->addrLineEdit->text(), 8888);
        connect(socket,SIGNAL(connected()),this,SLOT(on_connected()));
        connect(socket,SIGNAL(disconnected()),this,SLOT(on_disconnected()));
    }
    else {
        if(timer->isActive())
            timer->stop();
        socket->disconnectFromHost();
        delete socket;
        socket = nullptr;
    }
}

void Widget::on_connected()
{
    ui->connectPushButton->setText("disconnect");

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(updatepic()));
    timer->start(50);
    pixmap = new QPixmap();
}

void Widget::on_disconnected()
{
    ui->connectPushButton->setText("connect");
    delete pixmap;
}

char picbuf[1024 * 1024];

void Widget::updatepic()
{
    char c = 'p';
    int piclen = 0;
    int readn = 0;

    socket->write(&c, 1);
    socket->waitForReadyRead();
    socket->read((char*)&piclen, sizeof piclen);
    //qDebug()<<piclen;

    while(readn < piclen)
    {
        int bytes;
        socket->waitForReadyRead();
        bytes = socket->read(picbuf + readn, piclen);
        if (bytes < 0) break;
        readn += bytes;
    }
    pixmap->loadFromData((const uchar*)picbuf, piclen, "JPEG");
    ui->label->setPixmap(*pixmap);
}
