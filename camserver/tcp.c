#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/wait.h>
#include <arpa/inet.h>

#define MAX_BACKLOG	1024

int tcp_server_init(const char *ip, const char *port)
{
	int ret;
	int listenfd;
	int opt = 1;
	struct sockaddr_in srvaddr;

	memset(&srvaddr, 0, sizeof(struct sockaddr_in));
	srvaddr.sin_family = AF_INET;

	if (ip != NULL) {
		ret = inet_pton(AF_INET, ip, &srvaddr.sin_addr);
		if (ret != 1) {
			fprintf(stderr, "server->ip: ip is error\n");
			return -1;
		}
	} else
		srvaddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if (port != NULL)
		srvaddr.sin_port = htons(atoi(port));
	else {
		fprintf(stderr, "server->port: port must be assigned\n");
		return -1;
	}

	listenfd = socket(AF_INET, SOCK_STREAM, 0);
	if (listenfd == -1) {
		perror("server->socket");
		return -1;
	}
	setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt));

	ret = bind(listenfd, (struct sockaddr *)&srvaddr, sizeof(struct sockaddr_in));
	if (ret == -1) {
		perror("server->bind");
		close(listenfd);
		return -1;
	}

	ret = listen(listenfd, MAX_BACKLOG);
	if (ret == -1) {
		perror("server->listen");
		close(listenfd);
		return -1;
	}
	signal(SIGPIPE, SIG_IGN);

	return listenfd;
}

int tcp_server_wait_connect(int listenfd)
{
	int connfd;
	socklen_t addrlen;
	struct sockaddr_in cliaddr;

	addrlen = sizeof(struct sockaddr_in);
	memset(&cliaddr, 0, sizeof(struct sockaddr_in));
	connfd = accept(listenfd, (struct sockaddr *)&cliaddr, &addrlen);
	if (connfd == -1) {
		perror("server->accept");
		return -1;
	}

	return connfd;
}

